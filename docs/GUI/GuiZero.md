guizero is designed to allow new learners to quickly and easily create GUIs for their programs.

If you can download and unzip a file, you can install guizero - no special permissions or administrator rights are required.

If you have administrator rights and are connected to the internet, you can use pip to install or upgrade guizero (recommended).

Windows users can also use the Windows MSI installer.

https://lawsie.github.io/guizero/