## Hello world
```python
name = input("What is your name ?")
if name != "pierre":
	print("Hello " + name)
else:
	print("You are awesome!")
```


## Loops
```python
# While loop
while condition:
	# Code

# For loop
for in in [1, 2, 3]:
	# Code

```

## Led Pannel Transition

You can create some transitions for the led pannel
```python 
def transition(speed):
	"""
	where r, g, p and b are colors
	"""
    i=0
    while i<8:
        j=0
        while j<8:
            if(i%2!=0 and j%2!=0):
                hat.set_pixel(i,j,r)
            if(i%2!=0 and j%2!=1):
                hat.set_pixel(i,j,g)
            if(i%2!=1 and j%2!=1):
                hat.set_pixel(i,j,p)
            if(i%2!=1 and j%2!=0):
                hat.set_pixel(i,j,b)
            sleep(speed)
            hat.flip_h() #this function flip the matrix 
            j=j+1
        i=i+1
#endTransition
```

## Packages
```python
from time import sleep

# Make a pause
sleep(1.5)


# To use the SensHat
from sense_hat import SenseHat
hat = SensHat()
hat.low_light = True
hat.show_message("hello world")

# To clear the hat
hat.clear();

# Change custom pixel
hat.set_pixel(0,0,[255,255,0])


pixels = hat.get_pixels();
print(pixelsx)


# Images (PIL library)
from PIL import Image, ImageDraw
img = Image.open("image.png")
img_small = img.resize((8,8), Image.BICUBIC);
img_data = list(img_small.getdata())

for i in range(64):
	pixels[i][R] = img_data[I][R]
	pixels[i][G] = img_data[I][G]
	pixels[i][B] = img_data[I][B]
hat.set_pixels(pixels)

img = Image.new("RGB", (8,8), (0,0,255))
img = Image.new("RGB", (8,8), "darkGreen")
hat.set_pixels(img.getdata())

img = Image.new("RGB", (8,8), "darkGreen")
draw = ImageDraw.Draw(img)
draw.rectangle([(1,1), (6,3)], fill="red", outline="blue")
draw.line([(0,0), (4,0), (7,7)], fill="yellow", width=2);
hat.set_pixels(img.getdata())

from Math import cos, sin, pi

a = 0;
while True:
	a += hat.get_orienation_radians()['yaw'];#pi/8
	dx = cos(a)*5
	dy = sin(a)*5
	draw.rectangle([(0,0), (7,7)], fill="black")
	draw.line([(3,3), (3+dx,3+dy)], width=2);
	hat.set_pixels(img.getdata())
	sleep(.2)
```

```python
from sense_hat import SenseHat
from time import sleep

hat = SenseHat()
while True:
	# Get current orientation
	print(hat.get_orientation())
	
	# Get accelerometer data
	print(hat.get_accelerometer_raw())
	
	# Get current temperature
	print(hat.get_temperature())
	
	# Loop
	sleep(0.3)

# To use stick
evt = hat.stick.wait_for_event()
print(evt)
```


## Authors
* Daurelle Timothée
* Hubert Pierre
