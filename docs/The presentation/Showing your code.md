# How to show your code during the presentation.

## The important part:

- It's useless to show all the code, the presentation is 15 min, don't waste 10 min telling us how you made a for loop.
- Present rapidly the library used.
- A quick explanation about each class and their main function.
- And an explanation about the global structure of your application.


## How to present it:

- You can show us the raw code but it's always better to just add the importants part to your powerpoint with a little description. It's easier to read 20 line of code instead of finding what your talking about in the middle of 100 line.


## Question

- Be ready to answer question about your code, ``<<Why like this?>>``, ``<<Isn't it better to do like that?>>``, ``<<what it's doing?>>``. If it is a part of code you found on internet, be sure to know how it work and be able explain it.
- Be ready to answer how much code is from yourself and hox much from internet.