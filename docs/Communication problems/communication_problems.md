🛑[**WORK IN PROGRESS**]🛑

# Communication & organization : tips & mistakes not to make



## Introduction (Daniel)

In this part, we will try to give you some advice based on the mistakes we have made. Communication and organisation are aspects you really shouldn’t neglect : **the mistakes you make in those areas are the ones that, in the end, cost you the most time, and the majority of projects that don’t work out well are due to organizationnal errors.** 💣

This isn’t supposed to be a *magical recipe that makes your project succeed* , and neither do we imply that you should follow **all** of our advice. However, implementing just a few points from this part can definitely make you more efficient and help you avoid some of the mistakes that we have made, which will clearly make a difference. 🥇 You may think those things will make you lose time not coding, but trust us, in the long run, they’ll definitely give you more time to code and help you create a better product. 

To sum it up, we recommend that you read the different ideas that are in the titles, and pick whatever sparks your interest. ***It can only make your project better !***



## Collaborative tools (Hana)

* **git**  
* drive, slides, docs & equivalents 
* A discord or social networks (messenger, telgegram, slack....)
* Trello to organize tasks & ideas





## Weekly “meetings” (Guillaume)

* Every week, define and attribute everyone’s tasks 
* Should talk and meet up before each session 
* Test things before working every week 
* Framapad  => before / after => recap + Documentation

* Define a weekly organization / routine / workflow and try to stick to it



## When starting the project (Jean)

* Method : define your tasks, identify and make a plan of attack before coding 
* Define the specifications of the project before getting coding
* Prepare the architecture of the project and the main logic before coding 
* Don’t spend too much time on the beginning of the project
* Working version a few weeks before to work on the presentation earlier + not as much stress, can increment on it afterwards

Here are a few tips we’ve learned from our experience that should help you to pick the right project :

* First you have to know that you can’t spend too much time choosing your project. Dont go too fast but dont take more than 35 minutes because you have so a lot of other things to do. If you want to know how to select your project go to the click here.

* After that you have to prepare the architecture and the main logic of the project BEFORE starting to write code. It’s a really important thing to do because it will make you more mindful about what you want to do and how. Furthermore, it will help every member understand how your files will be organized, and how your general logic will work.

* When you finish this task remember something : you currently don’t precisely know what you’re going to do. You need to define different tasks and distribute them among yourselves. By doing it you are going to know all the task that the group need to do and what your personally need to do. If you can and think is useful you can define time for all the task that you are going to do and try to respect it.

Now that you’re ready, you can start coding !



## A few ideas for a modern organization (Daniel)

This project can be a great opportunity to try out some new organisationnal techniques, while also making your team more productive. If this is a topic that interests you (it will very probably be a huge part of your future job !), we definitely recommend to try some of these ideas out, or maybe apply some techniques you’ve heard or read about. 💡

However, always keep in mind to adapt any method to your needs and always do what works best for you ! **Sticking to a method that doesn’t help your team out is never a good idea.**

### Make your project as modular as possible.

This isn’t a “*modern organizationnal techniques*”, but it is certainly the most important advice we can give you : making your project modular will save you a lot of organizationnal conflicts and problems, and it will definitely make you more efficient.

This is the most simple way to approach this :

* **Clearly** define the different parts of your project (*this can be modules, functionnalities, screens, programming tasks...*).
* Once your project is split into different parts, let every member pick one or two **they like** (! this is important : it’s never a good idea to make someone work on something they don’t like). 
* **Develop every part as a separate project until you have at least two working parts**. They don’t have to be finished, they should be “*minimal viable products*”.
* **Then create the “Main” project : it is the one where you will merge the different parts.** Try to merge the two working parts into it. While doing so, the other members will be able to continue working on their parts.
* You’ve done the most difficult part 🙂 **From now on, just keep on merging new working parts & update the code of the already merged files. This will make your life a lot easier !**  

### Peer programming makes the difference. (Felix)

* Peer programming significantly reduces these expenses by reducing the defects in the programs. Pairs typically consider more design alternatives than programmers working alone and arrive at simpler, more maintainable designs. They also catch design defects early.

### Don’t be afraid of doing code reviews ! (Felix)

Do it safely !

* Do not hesitate to test your code as many times as you want. Do separate files just to test your "brick" of code. It's a safe way to progress in your work. 

### Applying agile principles has never hurt anybody. 

Scrum, kanban : not completely, but the elements that you find good

* Define your approach (cf our schema).
* Keep track of what all the group is doing and has done.
* Don't hesitate to get help from other members of the group when you have a problem.




## Other issues ? 


