# Run your program on start-up

## Rc.local

The /etc/rc.local file is executed on start-up of the Raspberry Pi, which means you can edit it to make it run your own program :

~~~bash
sudo nano /etc/rc.local
~~~

All you have to do is to insert your command before the "exit 0" at the end of the file :

```bash
python3 /home/pi/your_python_script.py &
```

**Be careful** : As your program will surely run continuously, don't forget the **&** at the end of the command as it could block the Raspberry Pi's boot.

Depending of the version of Raspbian, it might be possible that the rc.local is not executed, preventing your script from starting at boot. Furthermore, if your program relies on services that are not available when it starts, your program might have some difficulties running.

If you encounter any issue with rc.local, see systemd.

## Systemd

The other solution is to create a service that will start at boot and execute your script.

Move to the /etc/systemd/system folder :

~~~bash
cd /etc/systemd/system
~~~

Create a file with the .service extension and edit it :

~~~bash
touch your_python_script.service
sudo nano your_python_script.service
~~~

Here is a template for the file content :

~~~bash
[Unit]
Description=My service
After=network.target

[Service]
ExecStart=python3 your_python_script.py
WorkingDirectory=/home/pi
StandardOutput=inherit
StandardError=inherit
Restart=always
User=pi

[Install]
WantedBy=multi-user.target
~~~

The important lines to edit are **ExecStart** and **WorkingDirectory**. We're supposing here that your file is in the /home/pi folder.

Finally, you just need to enable the service so that is starts automatically when the Raspberry Pi boots :

~~~bash
sudo systemctl enable your_python_script.service
~~~

