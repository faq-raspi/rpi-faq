# Automatic dependencies and libraries updates + Application launch



Sometimes, libraries you are using can be updated or can be missing on the raspberry pi and you need to download them again in order to run you application. A simple way not to care about updates is to create a shell script to look automatically for updates before running your program.

For instance, the project Funny Driving Partner is using an "update_and_start" script :

~~~bash
# Speaker module ( in order to use hardware based python dependencies)
pip install gTTS;
sudo apt install mpg321;
sudo apt install flac;
sudo apt install libasound-dev;

# Speach recognition module ( "pure" python libraries)
pip install SpeechRecognition
sudo apt-get install python-pyaudio python3-pyaudio
pip install pyaudio
pip install pygame

# AT THE END -> launch our python program !
python ./Menu/main.py

~~~

With this kind of script, you don't need to care about missing or outdated libraries anymore ! 

Here's a little model for you to help you  be sure you won't have any dependencies issues anymore :

```bash
#First Part : Python-Hardware dependencies used.

#Second Part : Python Librairies used.

# Third Part : Launch of your application.
```
