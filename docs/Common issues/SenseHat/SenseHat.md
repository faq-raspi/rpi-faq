# Using the Sense Hat

The Sense Hat is a component that goes on top of the Raspberry Pi, with various sensors and an 8x8 led panel.
 To use it, you can use various methods, explained below.



## Importing the package

You need to import a package to make it work. "SenseHat" is an object that has methods to interact with the elements of your Sense Hat.

```python
from sense_hat import SenseHat

// Initialize the SenseHat
hat = SenseHat()
```



## Manipulating the LEDs

You can change the color of each led on the Sense Hat with set_pixel(int, int, int[3]). The 3 parameters are :

- **int:** the column number on the grid (in [0;7])
- **int:** the line number on the grid (in [0;7])
- **int[3]:** the color in RGB

```python
hat.set_pixel(0,0,[0,255,0]) // First number is column, then row, then the RGB color
```



You can store the pixels (8x8 panel = 64 pixels) in an array.

- **WARNING:** the array has 1 dimension, each line are put one after the other).

```python
pixels = hat.get_pixels() // We store all the pixels from the sensehat
```

After having modified your array, you can update the pixels like this :

```python
hat.set_pixels(pixels)
```



## Displaying an image on the led panel

SenseHat has a specific set of functions to display a **png** on the led panel :

```python
from PIL import Image

img= Image.open("alien.png")
img_small = img.resize((8,8),Image.BICUBIC) //make the image fit the panel
img_data = list(img_small.getdata())
for i in range(64): //modify the pixels accordingly
	pixels[i][R] = img_data[i][R]
	pixels[i][G] = img_data[i][G]
	pixels[i][B] = img_data[i][B]
```

You can also do something more impressive, like this :

```python
import time

img = Image.new("RGB",(8,8),"black")
draw = ImageDraw.Draw(img)
draw.rectangle([(1,1),(6,3)], fill = "red", outline="black")
draw.line([(0,0),(4,0),(7,7)], fill =(128,128,128), width =2)
a=0
while True:
	a+= pi/8
	dx = cos(a)*5
	dy = sin(a)*5
	draw.rectangle([(0,0),(7,7)], fill="black")
	draw.line([(3,3),(3+dx,3+dy)], width=2)
	hat.set_pixels(img.getdata())
	sleep(.2)
```



## Joystick

You can add a kind of ActionListener on the joystick to detect any event on it :

```python
int(event) = hat.stick.wait_for_event()
if event.action == "pressed":
	if event.direction =="down":
		// Write here your code
```



## Accelerometer

The accelerometer give you the vector that point the ground.

![vectorSenseHatAcceleration](./vectorSenseHatAcceleration.png)

You can test how it works with this program :

```python
from sense_hat import SenseHat
sense = SenseHat()

while True:
  acceleration = sense.get_accelerometer_raw()
  x=acceleration['x']
  y=acceleration['y']
  z=acceleration['z']
  print("x : "+str(round(x,1))+"     y : "+str(round(y,1))+"     z : "+str(round(z,1)))
```



## Sensor

* Get the pressure : 

```python 
pressure = int(hat.get_pressure())
```

* Get the temperature (of the Raspberry): 
```python
temperature=int(hat.get_temperature())
```



## Led Editor program

https://www.mediafire.com/file/dff9b2012rqb615/Image_editor.py/file

This program allows you to use an 8x8 grid to make images and export them.

### What's necessary ? :

- have **tkinter and numpy** plugins
- you can download **Thonny** (if it's not already installed) : it's a simple IDE for beginners

### How does it work ?

You can select your color on top right square and you can change the color of each pixel in the grid.

When you have finished, you can export it with the **export button**. An array appears on the python console. You can copy this array in your code !



* Authors : GEOFFROY, HUBERT Joseph, BRUN, ROJKOVSKA, FAGGION
