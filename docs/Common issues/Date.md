# Fix the date
If the date is not correct in the Raspberry Pi, HTTPS websites might not work.

Here is how to fix it (adapt the date to the current one):

Open a terminal and type the following command:
```bash
sudo date --set="2 OCT 2006 18:00:00"
```

Or you can also try:
```bash
sudo hwclock --set --date="2011-04-19 20:45:05"  --localtime
```

Or even another way:

1. Open [http://mirrordirector.raspbian.org/raspbian/pool/main/n/ntp/](http://mirrordirector.raspbian.org/raspbian/pool/main/n/ntp/) and download the latest version of `ntpdate`. The file should look like `ntpdate_4.2.6.p5+dfsg-7+deb8u2_armhf.deb` (download the file with the latest version)
2. Update the date: `sudo ntpdate ntp.ubuntu.com`


Author: Pierre HUBERT