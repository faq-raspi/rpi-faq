# Touchscreen FAQ (G2S4)

## How does it work?

First, we have to plug the touchscreen. Then, you can plug keyboard and mouse. Simple, isn't it?

### Keyboard configuration

Sometimes, keyboard configuration is **wrong** and is in QWERTY. You have to go in Configuration to change it to AZERTY, by following:
**Path:** Click on the top left RPI icon, then "Preferences -> Keyboard and Mouse -> Keyboard tab -> Keyboard Layout button -> Layout to French.

It should work. Otherwise give it a try by changing "_Variant_" into "_French_".

### HDMI cable

Keep in mind the touchscreen is attached to the Raspberry Pi, meaning you **can't separate** the screen & the core product. 
It also means you **can't plug a HDMI cable** and have a dual "screen": you have to code on the little touchscreen, and you can't plug the RPI to another place.

## Coding

### Using an IDE

To make code, we suggest using Python since it's a simple language. A default Python IDE is on the RPI, called _Thonny_ which is really useful. Thonny can be found at:
**Path:** Top Left RPI icon (Left Click) -> Programming -> Thonny Python IDE


If you want to do Java, an IDE is also provided and can be found at:
**Path:** Top Left RPI icon (Left Click) -> Programming -> BlueJ Jave IDE


However, since the screen is small and the machine not really powerful, it can be very slow to slide on the code if its thousand lines.

### Thonny is blocked

Sometimes, Thonny can be definitively blocked and even waiting will change nothing on the state of the IDE. You have to shutdown/restart the RPI, by doing:
**Path:** Top Left RPI icon (Left Click) -> Shutdown


### Saving your work

You **should save** your work on a USB key. You can also work directly on the USB key but don't forget to retrieve your work at the end of the English course, otherwise you could lose data.


### Adding sounds

If you need sound, you need a sound card (blue device) in USB. You have to connect your earphones to that blue device, and go to the Options:
**Path:** Right click on the Sound button on the top right -> Audio Output -> USB PnP sound device.
Repeat that to check if USB PnP sound device is checked. If Audio Output is not in the list, then try to unplug and plug again the sound card.


Unfortunately, some Raspberry PI in the English course can't work even if sound parameters are set properly. Don't lose too much time trying to launch audio sounds, because it could never work.

## Suggestions

I suggest coding on a computer using a Python IDE, and then export the code to the RPI touchscreen to test if everything is working (with resolution, optimization, issues).
This way you work on a big screen, which is useful to read code examples, or read that FAQ for example.
Plus, touchscreen is really small, and it's complicated to work in group and make you all see what's being done. If you are not convinced, 
touchscreen is no-blue light filter, so take care of your eyes!


## Final oral

For the final oral, prepare it. Since HDMI connections don’t work as stated above, you can’t project your production so take a video of it, or try to export it on a Personal Computer to make you can show the game in the class.
Otherwise, interaction will be killed because you can’t show the game to people when it’s on that little touchscreen.
You could also, for interactions again, say you want a student to come in front of the touchscreen to test what you have done. 

## Authors

Made by RANCHON Dorian, THOBOIS Antonin, SENER Emre