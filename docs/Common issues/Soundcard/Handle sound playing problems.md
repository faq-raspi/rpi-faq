# How to play an MP3 or WAV files ?

## Quick explanation of our project & the sound problem

With my group, we wanted to create a LaunchPad using the TouchScreen. To do so, we had to play a specific sound each time a button was pressed. By using the `pip3` python package manager, we installed a module called `playsound`. It can play MP3, WAV and probably other compatible sound files. But on our RaspberryPi, playing sound was impossible. Either we got a complex error concerning `Gstreamer` (a library of codecs) that was unsovable, even by updating the **gstreamer package**, or the internal soundcard wasn't detected. As a solution we tried to use the USB SoundCard available in you kits, but it didn't work either.

## The solution our group found

We imported the python module `os` (check the next example). The command `os.system("A_SHELL_COMMAND")` allows you to execute the command specified as a string parameter in a subshell managed by Python. 

Concretely, in our program, we used the command `clvc NAME_OF_A_SOUND_FILE --play-and-exit` to play our sound file. Here is a brief description of what this command does :
* `clvc` allows you to use VLC inside a terminal
* The `--play-and-exit` parameter will ensure that `clvc` is properly exited after the sound is done playing

There may be a better way to solve this problem, but we didn't find it. Feel free to try different solutions, or use this one if it works for you.

See the following example to understand how to use this solution.

```python
import os
os.system("clvc thank-you-s4-students.wav --play-and-exit")
```

## Consequences of using such a solution

The main problem of this solution is the delay that will be caused because of the following steps that are needed when you execute that command :
* Spawn a subshell
* Load vlc in the shell
* Initialize audio
* Decode the audio file
* Play the audio file
* Exit everything

The delay will be approximately between half a second and a second. But still, if you absolutely can't play a sound using a normal library, you may be saved and at least get a result.

## Other solution (with Pygame)

Another group tried to play sound. We also tried to record sound with the USB SoundCard and a microphone but the configuration was too difficult and too long so we only used the internal soundcard.

Instead of using `playsound`, we used the library `pygame`. This solution is preferable if you want to execute your audio inside of your Python program and it can play WAV, MP3 and OGG files.

How to play a sound :

**1.**You have to load the sound that you want to play :

```python
import pygame
pygame.mixer.music.load("path_to_your_sound.type")
```

**2.**Then you can play your sound and then use the function sleep(x) to play your sound for x second(s) :

```python
pygame.mixer.music.play()
from time import sleep
sleep(10) #Play the sound for 10 seconds
```

**3.**After that you can just wait the end of your sound or you can stop it like the example bellow :

```python
pygame.mixer.music.stop()
```

**4.**Finally, it's recommended to unload your sound, thanks to that you free up resources :

```python
pygame.mixer.music.unload()
```

You can also create an object Sound instead of load and unload a sound. Example :

```python
import pygame
from time import sleep
soundObj = pygame.mixer.Sound("path_to_your_sound.type")
soundObj.play()
sleep(1) #Play the sound for 1 second
soundObj.stop()
```

You have all the functions of pygame.mixer.music on this website :
https://www.pygame.org/docs/ref/music.html

You are now fully able to play sound with your Raspberry Pi ! Enjoy it !

*Author: Mathis Chapuis, Eric Rodriguez*