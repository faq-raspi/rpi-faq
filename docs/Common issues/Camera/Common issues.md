# Camera


## How to get started

- First, we have to check that the camera is properly plugged in.
- The raspberry pi's micro SD card should not be full.
- If it is full you should probably ask your teacher to change material.

## Common issues with camera 

- Be careful about how you are executing your previews:
- to get started : initialize the PiCamera Object ==> [camera_name] = PiCamera()
- resizing for fitting to the screen : camera.start_preview(fullscreen=False,window=(100,200,1000,800))
- if you are looking for PiCamera functions, read this: https://picamera.readthedocs.io/en/release-1.10/recipes1.html
- To start displaying camera view on your screen : [camera_name].start_preview()	
- To stop displaying camera view on your screen : [camera_name].stop_preview()
- Set a delay to trigger the end of the preview. If you don't you will not be able to see the preview.
- to shutdown ==> [camera_name].close()
- If your camera is not working, you need to check if you have enabled the camera in: sudo raspi-config (in terminal)->interface config->camera->choose yes.
- Don't forget to import librarie "picamera" to use the camera.

## Camera motion detection

- Function you will find on the internet are not usefull because you will work on python 3 and functions are mostly for python 2.
- Some libraries for the motion detection need to have motion sensor to run and work.
- Motion detection function that were working on python 3 were not returning us expected values so we had to make our own function to resolve the problem of motion.
- You can use motion library to detect change between 2 images, record video....
- To install it you need to: 
- sudo apt-get install motion --> to install it;
- sudo nano/etc/motion/motion.conf --> configure it.
- If you want more informations : https://www.supinfo.com/articles/single/311-camera-ip-avec-raspberry-pi-motion.
- The problem with motion is that everything is already done and will not have to code the functions.
- All you need to do is to configure the motion.conf file with what you need.
