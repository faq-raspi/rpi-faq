# Sound of colors

**What is this project ?**
* Application that allows us to play a 'song' related to an image
* You take a picture with the camera, then de Pi play a song 
* For each pixel that appears on the sense hat, the pi plays a note
* It creates a new song for every picture taken

**Libraries used ?**
* PyGame

**What did you learn ?**
* improving python skills

**Level of difficulty ?**
* Not very difficult, you just have to setup your devices correctly and it should work

**Motivating ? Why ? Why not ?**
* It was a nice project and the result was very funny

**Main difficulties**

* The sound card was not always working
