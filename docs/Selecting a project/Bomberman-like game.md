# Bomberman-like game

## Introduction

We made a bomberman-like game with Pygame and a touchscreen.

We had to deal with making explosions, moving our character, etc...

## Issues

The most challenging thing was to organize everything in order to have a coherent program.

We also tried to use sounds as inputs, but we weren't able to do it.

