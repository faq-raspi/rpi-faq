###Funny Driver Partner

**What is this project ?**

* A driving partner executing actions based on voice called functions, like detecting anger of the driver, playing music

**Libraries used ?**

* PyAudio
* Pygame
* SpeechRecognition

**What did you learn ?**

* OOP programming in python
* Facts about sound physics in order to measure sounds and recognize anger.

**Level of difficulty ?**

* Medium : some features have been hard to implement.

**Motivating ? Why ? Why not ?**

* As every project, it becomes boring when you're struggling, but the times everything was working were giving some motivation to go further.

**Main difficulties**

* We had some technical difficulties with the speech recognition, think about getting a good mic before using speech recognition features.
