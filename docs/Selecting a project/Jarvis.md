## Jarvis

**What is this project ?**

* Vocal assistant like Google Home or Alexa
* Creates reminders, reads your last emails, tells you the weather
* Started creating a functionality to send emails

**Libraries used ?**

* Jarvis project available here :  https://github.com/alexylem/jarvis.git 
* Snowboy's voice recognition
* SVOX Pico's text-to-speech

**What did you learn ?**

* Use and extend existing projects
* Programming in bash (and a little in Python)

**Level of difficulty ?**

We had a lot of trouble making the project work but by choosing the right libraries and a reasonable number of functions to implement, it is doable.

**Motivating ? Why ? Why not ?**

* Many plugins available thanks to the community, motivating to create our own plugins
* Vocal recognition worked most of the time but randomly bugged, making us loose time on trying to fix it

**Main difficulties**

* Getting accustomed with the project and the libraries we were using
* Project basically coded in bash, unintuitive language unlike Python