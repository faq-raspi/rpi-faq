# Tilted Ball (Hugo, Marin)

Tilted Ball is a game where you tilt the sense hat to move the ball to the right and the left so that the ball can avoid pikes and holes.
This project had been made by Hugo, Marin and Pierre, and we're going to explain here how we proceeded, and how it went.

## Beginings

* We started by create a **git**. Creating a git is **essential**.

* We chose an idea of project.

* We tested all together technologies of Raspberry Pie we didn't know so far.

* We split the job in three parts to work three times faster.

* We enjoyed. At least a bit.

## In general

We used to work "on our sides", however, we've always explained eachothers what we did twenty minutes before the end and **commited** our changes.
That way, we could work as fast and efficiently as possible while doing the project as a group.