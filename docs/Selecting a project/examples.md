# Examples of project

### Toaster Machine

**What is this project ?**
* Application where you can create a beat 
* It contains 2 librairies of sound 
* You can place them in a loop in order to create the beat you want
* You can change the loop speed (increase or decrease from 30 to 190 bpm)

**Libraries used ?**
* PyGame, Tkinter

**What did you learn ?**
* improving my python and tkinter level -> learn new method ...
* use thread in the code for the loop 

**Level of difficulty ?**
* It was a little bit diffucult especially for the sound -> it was not always working 

**Motivating ? Why ? Why not ?**
* It was interesting to do a project with sound and you don't have always the opportunity to make a project about music

**Main difficulties**

* The sound card was not always working


### Sound of colors

**What is this project ?**
* Application that allows us to play a 'song' related to an image
* You take a picture with the camera, then de Pi play a song 
* For each pixel that appears on the sense hat, the pi plays a note
* It creates a new song for every picture taken

**Libraries used ?**
* PyGame

**What did you learn ?**
* improving python skills

**Level of difficulty ?**
* Not very difficult, you just have to setup your devices correctly and it should work

**Motivating ? Why ? Why not ?**
* It was a nice project and the result was very funny

**Main difficulties**

* The sound card was not always working

