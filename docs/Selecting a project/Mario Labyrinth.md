### Mario Labyrinth

**What is this project ?**
* It's a basic physical skill [labyrinth game](https://bit.ly/39zcJlr "Wikipedia"), you have to avoid holes and walls in order to arrive to the end
* We took advantage of the Sensehat accelerometer to move the ball on screen like a Wiimote Mario Kart controller
* Our game design is using the Donkey Kong arcade game sprites: holes are the barrels, and the ball is mario's head

**Libraries used ?**
* PyGame, Tkinter, SenseHat

**What did you learn ?**
* It improved our python level
* We learned about OS level issues on linux

**Level of difficulty ?**
* It wasn't that hard because all members of our group already did a video game on python. It allowed us to add several unplanned fonctionnalities

**Motivating ? Why ? Why not ?**
* We had a lot of fun: every day with english class was a nice day to look upon

**Main difficulties**
* We didn't encounter hard difficulties or any raspberry pi or python general difficulties
