***Facial Recognition system***

- It tells your average TOMUSS mark thanks to a text to speech
- It gets the grades from TOMUSS with the RSS feed
- It shows your name and your grade next to your face
- A square appears around your face, if it's green your average is upper than 10

**Libraries used ?**

- Open CV, festival, PiCam

**What we learnt**

- Improved our Python skills
- How a facial recognition system works (globally)

**Level of difficulty:**

- It was not that hard because we followed tutorials to make that project possible
- The real challenge was assembling all the functionnalities we made

**Motivating ?**

- The start was quite complicated because we had some installation problems while we couldn't programm anything.
- The project was ambitious and fun so it was motivating

**Main difficulties**

- Installing OpenCV was very long
- The facial recognition was sometimes very slow (Pay attention to the use of threads and try to separate your functionnalities, for example we had to separate TTS and facial recognition)