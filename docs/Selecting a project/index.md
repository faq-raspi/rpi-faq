# A way to find an idea
1. Choose the devices you want to use
2. List what functionalities it have (like leds for sensehat)
3. Find a topic (like video games,...)
4. Find idea of project related to the topic and with you can add functionalities of the device

### 1.How to choose the device
* Simply choose the device you desire to work with 
* Choose by the functionalities of the device : if you are interested on using a particular fonctionality

### 2. List functionalities of the devices you choose
* Listing those functionalities can help you to find an idea because it will allow you to see what you can do with it.

### 3. Find a topic
* You can find a topic according to your motivation, that inspires you :
    * A topic you like/enjoy
    * A topic you want to work on
    * A useful topic
    * A challenging topic

### 4. Find the idea
* After doing all these steps try to find an idea related to your project that will makes you able to use functionalities of the devices

### Or you can :

* If you don't have an idea choose something that can help someone (or yourself)
* Find some project on Raspberry Pi on Internet
* Start with something easy for the beginning, if you still have time, you can add some more complex functionalities
* Ask for ideas from other students
* Take time to look at the differents technologies and documentation to get a better idea of what you can do.
* Don't be afraid to use your time at the beginning of the project to search for a good idea
* [Click here to have a website with multiple idea](https://circuitdigest.com/simple-raspberry-pi-projects-for-beginners)



--------------------------------------------------------------------------------
Author: Maxime Pino, Hugo Forel, Bastien Laurans, Bastien Moronval
--------------------------------------------------------------------------------

Lucas MACE
Mario SA
Rémi SERPOLLIER
Hugo PEREZ
