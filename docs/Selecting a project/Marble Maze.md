### The Marble Maze 

We made a Marble Maze on the matrix by lighting the LEDs. This is a game where the player tilts the sense hat to make the marble move and to escape the maze.
The wall’s colour and the maze could change on every game, also there were one winning point and several losing points. 
The player can change the difficulty of the game by pushing the joystick up or down at the beginning.
We used three music, one during the game, one when you lose and one when you win. 
We also added winning and losing features such as dancing stickman and creeper.

### Libraries

#Sense Hat
We obviously needed the sense hat library to get the pitch and roll to modelize the movement of the ball.

#Pygame
We used the pygame library only to load and play the music.

#Time
We needed time library just for the sleep function.

#Random
To generate random number so we could load mazes randomly

### Issues

We had the idea of this project from internet, and first we followed a tutorial. 
That wasn't a really good choice because after 3 weeks, the game was already functional. 
We had to think about what we could add to make the game more interesting and it was difficult to change the rules of this well-known game.
So we had troubles to advance the project and we didn't have a lot of motivation at this moment.
Therefore, we decided to add some funny content such as music and animations.

 