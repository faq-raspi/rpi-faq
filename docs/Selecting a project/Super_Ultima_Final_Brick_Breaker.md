# Super Ultima Final Brick Breaker Game

The paddle is moveable by hand and all the bricks are randomly place in the game’s window. The ball bounced on all the brick’s edges. 
To make the game even more interesting, we added some bonuses and maluses such as a fire ball that destroys everything on its way, a slow mode and the mini paddle.
The number of levels is infinite and each time you succeeded one level, there’s one more brick in the game. 

## Libraries used ?
Mainly Tkinter.

## What did you learn ?
Work in team with some members that we have never work with before. 
Improving our Python skills by creating a game from scratch.

## Level of difficulty ?
It was not that hard but as the game is known, we decided to make it all by ourselves.

## Motivating ? Why ? Why not ?
We choose the project that we wanted to make so it was way more interesting. 
Also it’s a game that we all know and have played before so it makes it even more fun so it was motivating.

## Main difficulties
We encountered some difficulties with the soundcard that doesn’t work with the touchscreen 
so we weren’t able to add the background music as we wanted to create an 80’s arcade game atmosphere. 
Also, it’s was quite difficult to handle the right value to allows the ball to bounce on every axis and make the game realistic. 
