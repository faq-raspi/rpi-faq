Look at old project ideas (the ones in this file) to chose yours accordingly

Concerning the SenseHat :
- Be careful about the values, they're not always accurate (like the pressure)
- The bluetooth module is hard to use
- You gotta make sure your project is one that you can show during the demo at the end
- Don't neglate the final presentation : it counts a lot for the final mark
- Keep your framapad up to date and detailed it'll be useful for the final presentation
- Backup or die