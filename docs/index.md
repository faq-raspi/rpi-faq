# Raspberry PI FAQ
Welcome to this Frequently Asked Question (FAQ) website built to help you with your Raspberry Pi projects.

Hopefully you will find here the solution to the technical problems you will encounter during your project...

## Source code
The source code of this project is available at [https://forge.univ-lyon1.fr/faq-raspi/rpi-faq](https://forge.univ-lyon1.fr/faq-raspi/rpi-faq) 

## Make similar documentations
If you want to make a documentation similar to this one, you can follow [this guide](https://blog.communiquons.org/#/blog/create_documentation_mkdocs).