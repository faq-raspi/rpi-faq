# User guide for Github

This is a guide on how to use Git, you can use it for Gitlab or Github it works for both.

## Getting started with Github

### Creating a Github account
First of : https://github.com/ (go there).
Either create (very easy and free) an account or connect to your existing account.
Now that you have a Github account :

### Creating the repository
Click on new over the *Repositories* search bar, enter a name (one that makes sense) a short description of what the project is about, making it private or public is up to you and create a README (seriously that helps a lot). The README is a file that you'll edit as the project goes on, you have to add the functionalities and how the program works; it's a sort of guide. Then click on *Create repository*.

And there you go, you just created your git repository for your project.
And now to be able to use it you'll have to clone it.

## Creating and adding a SSH key(s) to Github

To clone your repository you'll need an SSH key.
To generate a ssh key :
	- Type the command 'ssh-keygen' in a command prompt 
	- Then copy the content of the file with the extension *id_rsa.pub*
	- DON'T COPY SHARE THE CONTENT OF THE OTHER FILE, IT'S PRIVATE INFORMATION (like you password or credit card number)
Now to add that ssh key to your github account :
	- Click on your profile picture
	- Select *Settings*
	- Then *SSH and GPG keys*
	- Select *new SSH key*
	- Add a title (name) and paste the previously copied content of *id_rsa.pub* file
	- And then click on *Add SSH key*

Now that you have added the SSH key to your github account you can clone the repository.

## Cloning the repository

Cloning the repository allows to modify and add files to the Github project from command lines.
On Github select the project in the repository list.
Then you should find a button *Clone or download*.
Select *Clone with SSH* then copy the link.

On linux : on a terminal type *git clone* and then paste the copied link.
On windows : in a git bash terminal type the same command (SHIFT+INSERT to paste).

And now you have access to your git repo in your local files.

## How to edit the repo

If you edit or add files, to also add it to the Github project, you'll have to type a few command lines :
	- *git status*
It'll show you what has been edited/deleted.
	- You'll have to use *git rm* and the name of the deleted file and/or *git add* and the name of the file according to the result of the *git status* command.
	- *git commit -m* and then the commentary of the commit; what you've changed, added or deleted (for group projects make it clear, writing *stuff* won't help).
	- To ensure that you don't overwrite other people's work always do *git pull*
	- and then push the changed into the Github project with *git push*

And then you and your collaborators should be able to see the changes on the Github project page.

## How to pull changes

Simply type : *git pull* .
It'll show you what have been added/deleted, may it be files directories or lines of code.

# Gitlab

If you want to use Gitlab, it works the same way.