### Emulators :  
  
**You can easly find few  emulators online :**   
An emulator can be really usefull for working at home, or even in class because only 1 Rpi is available for each group.
There exists few emulators for Rpi but even less works with tools like the camera or the touch screen.

- SenseHat : <a href="https://trinket.io/sense-hat">https://trinket.io/sense-hat</a>   
*you just have to paste your code in it

- The use of VMware and the raspbian OS
- You also have framboise314 or Qemu but theses doesn't support everything (These are based on olders version of raspbian)
- I Advice you to use VMware which with some configuration can do pretty much anything a rasberry pi can (Use of camera, microphone...)

* Authors :Timothée Daurelle, Gaillard Loïc
