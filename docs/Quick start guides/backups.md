# Backups

Backups are very important in your project because your **raspian** will be used by other groups.


## make install scripts
Once you found what libraries and packages you need, you should make a script that automates the instalation. One of the other groups might break the raspberry you are on, and having a script ready to be launched to reinstall all of what you need will accelerate the reinstall process, thus making you lose less time. For example if you had bluetooth, pygame, vlc and PIL to install you could make the following file:
```bash
#!/bin/bash
sudo apt install python-dev libbluetooth-dev bluez vlc
sudo python3 -m pip install pybluez PIL python-vlc
```
Also save a copy of your work after each session in your own USB sticks or computers and also on GitHub or GitLab. You don't want to lose hours of works because of a lack of backups.

## Git: The best way

The most efficient and safe way is to use git and GitHub/GitLab.

>Notice that you have already have a GitLab account with your student login and password `(pXXXXXXX)` on the ![Univeristy forge](https://forge.univ-lyon1.fr/).

For detailed informations about how to use GitHub or GitLab, check the ![Git guide](./gitguide.md).

### Use a git repository :
+ [x] Safe
+ [x] Available to **all** group members from **everywhere**
+ [x] Manage versions *(and so avoid lots of mistakes)*


+ [ ] Need to now how to use it *(come on it's easy)*

## USB key : a good complement 

Store your project on a **USB key** *(or SD card)* can be usefull in some cases but isn't essential.

However it's truly **ADVISE AGAINST** use a USB key **without** a git repository.

### Use an USB key :

- [x] Can't be easier to use
- [x] No connection needed


- [ ] Only one member own the project
- [ ] No possibility to recovery in case of deleting/breaking files
- [ ] Need to have it and so to don't forget it

Have a USB key with your project (in addition of an other solution) is always a good idea especially the day of your presentation, just to be safe.

## Google Drive :

This is not a solution, if you have think about that first whip yourself with a HDMI cable and then eat one by one the key of your key board and then go ![Here](./gitguide.md) to learn how to use GitHub or GitLab.

## Conclusion

Congratulation you now have all tools for backup your work correctly so now don't forget to do it !
> **Important :** *add*, *commit* and *push* at least one time by session

*Authors: Nicolas Raymond, V. Jallamion, Thomas Montero*