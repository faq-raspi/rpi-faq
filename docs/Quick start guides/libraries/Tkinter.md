# Tkinter library (Thibault)


## Why use Tkinter

Tkinter is a very usefull library, mainly used for the creation of graphical user interfaces (GUI) using a set of graphical components called widgets.<br>
It propose functions and classes to handle events (keyboard, mouse, ...), and display (images, buttons, bitmaps, drawing...).



## How to use Tkinter

Fortunately this library is already part of python 3 since quite a long time, so no install required !

Library to use :
```python
#===========================================================================#
    
"Import library:"

    from tkinter import * #allows you to use all the features of the library


#===========================================================================#
```


## Useful links

http://tkinter.fdex.eu/index.html</br>
https://docs.python.org/fr/3/library/tk.html</br>
https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html</br>


## Example of code

To initialise Tkinter (creation of the window and the canvas):</br>
*more at http://tkinter.fdex.eu/doc/caw.html#*

```python
#===============================================================================================#


"Window creation :"

    window = Tk() #create a tkinter window
    w,h = window.winfo_screenwidth(), window.winfo_screenheight() #extract the screen size
    window.overrideredirect(1) #force fullscreen
    window.geometry("%dx%d"%(w,h)) #maximize the window


#==============================================================================================#


"Canvas creation :"

    canvas = Canvas(window, width=1920, height=1080, bg="black") #create a black 1920x1080 canvas
    canvas.place(x=0, y=0) #we position the canvas in the top-left angle of the window


#==============================================================================================#
```


To draw shape :</br>
*more at http://tkinter.fdex.eu/doc/caw.html#les-lignes*

```python
#=====================================================================================================================================#
    

"Drawing :"

    #Triangle (3 lines):
    canvas.create_line(55, 85, 155, 85, 105, 180, 55, 85) #draw 3 lines on the canvas, here it makes a triangle

    #Polygon:
    points = [150, 100, 200, 120, 240, 180, 210, 200, 150, 150, 100, 200]
    canvas.create_polygon(points, outline='#f11', fill='#1f1', width=2) #draw a green polygon with a 2px wide red border on the canvas

    #...


#=====================================================================================================================================#
```


To display image :</br>
*note that images can be tagged, detected and much much more, look at the documentation for more informations,</br>
http://tkinter.fdex.eu/doc/caw.html#les-images*

```python
#=======================================================================================#


"Initialization :"

    img_test = PhotoImage(file="images/test.png") #link the image to a variable


#=======================================================================================#


"Creation :"

    test = canvas.create_image(960, 540, image=img_test) #display the image at (960,540)


#=======================================================================================#


"Move :"

    canvas.coords(test, 420, 0) #move the image 'test' to (420,0)
    canvas.update() #refresh the canvas; can resolve must of your problems


#=======================================================================================#


"Destruction :"

    canvas.delete(test) #delete the image 'test'


#=======================================================================================#
```


To use event with Tkinter :</br>
*you can find key names at https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html*

```python
#===================================================#


"Events :"

def left(event): #define a function 'left'
    ...
def right(event): #define a function 'right'
    ...
def jump(event): #define a function 'jump'
    ...


#===================================================#


"Key binding :"

#call the move functions when 'q' or 'd' are pressed
window.bind("<KeyPress-q>", left)                                   
window.bind("<KeyPress-d>", right)

#call the jump function when 'space' is released
window.bind("<KeyReleased-spacebar", jump)


#===================================================#
```

To leave Tkinter:

```python
#=======================================#


"Close the window :"

    window.destroy() #destroy the window


#=======================================#
```
