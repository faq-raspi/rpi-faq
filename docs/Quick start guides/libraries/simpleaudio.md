### Simple audio

Here is a simple tutorial to use the simple audio library, simple audio is useful to play sound asynchronously

1. First, you need to download the library with the command line `pip3 install simpleaudio`
2. Import it in your project by adding the line `import simpleaudio as sa` for exemple
3. Create a WaveObject  `soundFile = sa.WaveObject.from_wave_file("path/to/your/file.wav")`, you may need to import wave using `import wave`.
4. Play the sound by creating a PlayObject with like that `play_sound = soundFile.play()`
5. Your PlayObject will play in the background so your code execution will continue. If you want to stop the code execution until the sound is fully played, you need to use the method .wait_done(). You can also stop it using .stop() or check if the sound is still playing with .is_playing()


[Link to the full documentation](https://simpleaudio.readthedocs.io/en/latest/)

Exemple :
```python
import simpleaudio as sa
import wave

soundFile = sa.WaveObject.from_wave_file("path/to/your/file.wav")
play_obj = soundFile.play()
play_obj.wait_done()
```
 
Lucas MACE