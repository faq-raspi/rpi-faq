# Pygame library

## Why use Pygame

Pygame is a library to help in the creation of a game.<br>
It propose functions and classes to handle events (keyboard, mouse, ...), music and display.<br>
For more details about music and sound part, go to [Handle sound playing problems](http://faq-raspi.pages.univ-lyon1.fr/rpi-faq/Common%20issues/Soundcard/Handle%20sound%20playing%20problems/#other-solution-with-pygame).


## How to use Pygame

If pygame isn't installed, write on the terminal :

python3 -m pip install -U pygame --user 

Library to use Pygame : 
```python
    import pygame
```

## Useful links

https://www.pygame.org/docs/ 

## Example of code

To initialise Pygame : 

```python
    pygame.init()
```

To create a window : 

```python
    window = pygame.display.set_mode((750, 400)) #Create a window with height and width parameter
    
    pygame.display.set_caption("Project name") #To change the name of the window
    
    pygame.display.update() #To reload/update the display/window
```

To draw shape : 

```python
    pygame.draw.rect(window, (255, 0, 0), (x, y, 50, 50)) #Draw a rectangle on window, with the color (255, 0, 0), (left, top, width, height)
```

To display image : 

```python
    img = pygame.image.load("playButton.png") #To load the image playButton.png
    window.blit(img, (x, y)) #It display the image on window in the position (left,top)
```

For mor details : https://www.pygame.org/docs/ref/draw.html#pygame.draw

To play music and sounds:

```python
    pygame.mixer.music.load("elevatormusic.mp3") #The file elevatormusic.mp3 must be on the same directory that the python program
    pygame.mixer.music.play(-1) #The music loop indefinitely
    
    explosionSound = pygame.mixer.Sound("explosion.wav") #Create a Sound 
    pygame.mixer.Sound.play(explosionSound) #Play the Sound created
```

For more details : https://www.pygame.org/docs/ref/music.html#pygame.mixer.music

To use event with Pygame :

```python
    for event in pygame.event.get(): #It's a collection of the event that are catch by pygame
        if event.type == pygame.QUIT: #If the user want to leave the application
           ...
        if event.type == pygame.MOUSEBUTTONUP: #If the user has pressed mouse left button
            posX, posY = pygame.mouse.get_pos() #To get the position of the mouse
            if 175 < posX < 575:
                if 100 < posY < 150:
                    ...
                elif 250 < posY < 300:
                    ...
                elif 175 < posY < 225:
                    ...
```

To leave Pygame:

```python
    pygame.quit()
```
