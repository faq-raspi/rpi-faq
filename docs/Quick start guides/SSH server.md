Having multiple people working on the different files of programs can increase your speed, also you may be more comfortable working from your machine.   
To do so you can use the **SSH protocol.** 

**Your computer and Raspberry Pi should share the same network**

*  The command for it is installed by default on Linux (install the `openssh` package if it isn't).  
SSH allows you to open a terminal on the raspberry pi as if you were on it, to open graphical applications and to transfer files (among others).  


*  **Windows users can use **

1.  [PuTTY](https://www.putty.org/)  
2.  [MobaXterm](https://mobaxterm.mobatek.net/)  
3.  [Bitvise](https://www.bitvise.com/)  

*  **WSL (Windows Subsystem for Linux) users should use the same instructions as Linux users.**


*  Make sure SSH is installed on the raspberry type `sudo apt install ssh`.


*  Check if the SSH server is on on your raspberry pi : type `ssh localhost`. 
If it asks for a fingerprint or a password, it means the server is working.


*  To enable the SSH server, type `sudo raspi-config`, then navigate to the `Interfacing Options` tab, then select `Enable` next to `SSH` then select `OK`.  
Once that is done and checked, you can type `hostname -I` in the raspberry pi console. It will give you the address you'll use to connect to it.  


*  Then on your own Linux machine, you can type `ssh pi@<ip of the pi>`. You'll need to accept the fingerprint, and the default password is `raspberry` (the password will not be shown, not even masked, but it does register your keystrokes). 
*  You are now on the raspberry pi.  

*  With PuTTY, set the `Host Name (or IP address)` to the IP you got, use `Port` 22 and `SSH` type. When it prompts you for a username, use `pi`, and the password is `raspberry`. The password will be completely invisible when you type it, but it will still be registered.  
To use graphical applications, you'll need to use `ssh -X` instead of just `ssh`, and run the apps you need with a `&` (like `thonny&` to get a Python IDE) at the end of the command. 

*  On Windows or WSL, it also requires an X server running on your machine, but setting it up is out of this document's scope.  
To transfer files you can use `scp <local file> pi@<ip of the pi>:<remote file>` (or the other way around), or on Windows with [FileZilla](https://filezilla-project.org/).

