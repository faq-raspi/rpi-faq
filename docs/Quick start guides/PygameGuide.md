# Pygame guide

## Main Loop
You need a main loop to put your program in it.
You can manage the time between each iteration with the time library.

Here are the import you need :
```python
import pygame
```
To start using pygame, create a window by doing :
```python
pygame.init()
win = pygame.display.set_mode((750,400))#size of the display
pygame.display.set_caption("Window name")
```

Don't forget to close it at the end of your program :
```python
pygame.quit()
```

You can manage your main.py by doing something similar to (it’s the main loop) :
```python
import pygame

run = True
gameHasStarted = False

# The clock will be used to control how fast the screen updates
clock = pygame.time.Clock()

while run:
	while not gameHasStarted:
		for event in pygame.event.get():
			event.type == pygame.QUIT:
				run = False
	#Dispay Menu here
	pygame.display.update()
	#60 frames per second
    clock.tick(60)

	while gameHasStarted:
		for event in pygame.event.get():
			event.type == pygame.QUIT:
				run = False
	#Display game here
	pygame.display.update()
	#60 frames per second
    clock.tick(60)
```

The syntax to import a class from a file is:
```python
from ExplosionArea import ExplosionArea
```
So, ``from File import Class``

To display an image, you first have to load it in your program:
```python
self._shape = pygame.image.load("bombExploding.png")
```
And display it by doing:
```python
self.win.blit(self._shape, (self._x, self._y))
```
blit is a method from the window, the first argument is the image you loaded and the second is the position of the image in pixels considering that the top left corner of the window is (0, 0).
