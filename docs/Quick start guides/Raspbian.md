# User guide for Raspbian

## Useful commands
* `cd DIRECTORY_NAME` to change the current directory  
    - *Example:* `cd /home/pi`

* `ls` to see the content of a directory  
	- *Example:* `ls /home/pi`
    - *Output:*  
	<pre>
	Desktop     Downloads   opt        Public      Videos
	Documents   Music       Pictures   Templates  'VirtualBox VMs'
	</pre>
	- In most shells, as an output of the `ls` command, directories will be coloured, files will remain white, and executable files will have a different colour

* `rm -rf SOMETHING` to delete something **without confirmation**

* `cat FILENAME` to see the content of a file
    - *Example:* `cat english-s4.md`
    - *Output:*  
	<pre>
	Why are 2019-2020 S4 the best ? That is a very simple question. They shared love by making this concrete, complete and awesome guide for you.
	They are amazing people.
	</pre>

* `sudo apt update` to update the list of packages

* `sudo apt ugrade` to upgrade all the packages that can be updated

* `sudo apt install PACKAGE_NAME` to install a package

* `sudo apt remove PACKAGE_NAME` to remove a package

* `nano FILENAME` to edit a file inside the terminal
  - `CRTL+X` to exit a file
  - Then press `y`
  - Change the name of the file if you want to do so
  - Then press `enter` to confirm

* `systemctl reboot` to cleanly reboot the RaspberryPy from a terminal

* `systemctl poweroff` to cleanly shutdown the RaspberryPi from a terminal

* `python3 YOU_PYTHON_FILE` to execute a python script from a terminal

*Author: Mathis Chapuis*

## Access a text file and use it :
*  create an empty .txt file  near the python file

*  to access and read the text you've writen :  
<pre>
file = open("File Path","r")
str = file.readline() #return String
file.close()
</pre>

*  to access and write a string value :  
<pre>
file = open("File Path","w")
file.write(str)
file.close()
</pre>

*Author : Ruiz, Khemmar, Semlali*

## Some tips
* If the RaspberryPi TouchScreen's rotation is inverted, edit the file located at `/boot/config.txt` with `sudo nano`. Then locate and replace the line to `lcd_rotate=2`

* If it complains about privileges, use `sudo YOUR_COMMAND` to run it as a super user

*Author: Mathis Chapuis*