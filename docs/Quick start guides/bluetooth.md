# Bluetooth
Using bluetooth with the raspberrypi can be quite complicated to setup. The library used for it, PyBluez, is not always installed by default. To install it you'll need one step before to install the requirements, in a terminal run:
```bash
sudo apt install python-dev libbluetooth-dev bluez
sudo python3 -m pip install pybluez
```
The official documentation for pybluez can be found on: https://pybluez.readthedocs.io/en/latest/install.html

## Example
How to do a scanner with the bluetooth library ?
```python
from bluetooth import *


class Btooth:
	def __init__(self):
		self.device_list = []

	def lookUpNearbyDevices(self):
		self.device_list.clear()
		try:
			discoverer = DeviceDiscoverer()
			nearby_devices = discover_devices(lookup_names=True)

			device_list = []
			for addr, name in nearby_devices:
				deviceList.append((addr, name))
		except btcommon.BluetoothError:
			print("communicating error")
			device_list = []
		self.device_list = device_list
```
This programm use the function DeviceDiscover() to put all nearby devices in a table.