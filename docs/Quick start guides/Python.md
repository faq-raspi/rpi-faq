# Python

## Advices

1.Code should be easy to read<br>
2.Write a clearly code / make spaces<br>
3.Simplify your functions<br>
4.Create global Variabes<br>
5.Leaves some comments for others peoples understand your code<br>
6.Write your code with Visual Studio or and other good IDE (don't use notepad++)<br>
7.Organize the code : give a good architecture (classes, interface, class main)

## Some links to learn Python:

https://www.youtube.com/watch?v=psaDHhZ0cPs
https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python
https://python.doctor/


*author : TOUHARDJI Hamza, NINI Rayane , ABOUCHI Younes*


## Tkinter


*  Library to use Tkinter : from tkinter import *
*  Exemple of code to create a window :

        window = Tk()
        window.title("Title")
        
        ws = window.winfo_screenwidth() #get the width of the screen
        hs = window.winfo_screenheight() #get the height of the screen
        
        canv = Canvas(window) #create a canvas to draw something in
        canv.pack() #add the canvas to the window (same for all your objects : obj.pack() )
        
        label=Label(window,text="text as you want",fg="#ED961E", font="Helvetica 32 ", bg=background)
        label.pack()
        
        canv.create_line(x_start, y_start, x_end, y_end, fill="white", width=10)

*  To add an image you need the library 'PIL' and the function PhotoImage

        from PIL import image
        photo = PhotoImage(file='Path File')
        canv.create_image(width, height, image = img)
*  The window is creating once and you need a window.update() to make animations or change something

*author: Ruiz*


## Specificities of Python

To add comments you have two options :
* \# is used for a one line comment
* For multiple lines, you have to use 6 " and put the comment between the three first " and three last "

Here's an example of the use of both comment method:

```python
# This is a one line comment
    
"""
this
is
a
multiple
line
comment
"""
```

Variables in Python have no types
To initialise a variable you give it a name and a value

* Exemple of code to initialise different usual types:


```python
#the following initialise an int that has 69 as a value
lmaoNumber = 69
```
```python
#this is to initialise a float, you just add a coma also known as dot or "."
lmaoFloat = 42.0
```
```python
#the boolean type exists and you to initialise a bool with the true value you write True with a capital T letter, the same goes for a False value
boolTrue = True
boolFalse = False
```
```python
#to initialise a String that has "Hi, i'm a string" as a value you do this
politeString = "Hi, i'm a string"
```
```python
#characters don't exist in python, they are just Strings with a length of 1, you can use single and double quotes for them
charLetterA = "A"
```

In python, indintation is key and there is no code blocks in between {} unlike C or other languages
To know if you're in a block of code you have to indent the code at the same level

For example, in C to run a code on a condition that takes multiple lines you would do this:
```c
int a = 2;
int b = 3;
if(a>b){
    printf("A is greater");
    printf("A's value is: %d", a);
}else {
    printf("B is greater");
    printf("B's value is: %d", b);
}
```
In Python, this code would look like this:
```python
a = 2
b = 3
if(a>b):
    print("A is greater")
    print("A's value is: ", a)
else:
    print("B is greater")
    print("B's value is: ", b)
```
As you can see, there is no end of line like ";" nor blocks in beteen brackets {}
Also, the parentheses before the condition and after in the if aren't mandatory
So these two ocnditions are correct:
```python
if a>b :
    print("A is greater")
    print("A's value is: ", a)
else:
    print("B is greater")
    print("B's value is: ", b)
```

The && condition in C is and only in python:
```python
if 1>0 and 1!=0:
    print("1 is greater than 0")
```

The || condition is or:
```python
if 0>2 or 2!=0:
    print("2 is different than 0")
```

* Useful link: [https://www.w3schools.com/python](https://www.w3schools.com/python)


Authors : AL YACOUB Angelo, LAVERRIERE Fañch, DIOUF Samba Diarra and DUPORT Quentin G2S4


## Reading or writing inside files

*  Opening a file :

    `dc = open("file.txt", "rw")`
        
    Where *dc* will contain the *file descriptor* of the file you will open.
    Please note that the *second parameter* will contain the ways you want to open the file, here are the main parameters :
    *  r : opens the file in Read mode
    *  w : opens the file in Write mode
    *  a : appends the cursor to the end of the file
    *  x : creates the file if it doesn't exist<br><br>
    *Those parameters can stack up.*<br><br>
        
* Closing a file :

    `dc.close()`
        
    Where *dc* will contain the *file descriptor* of the file you want to close.
    
* About cursors :

    Python works in the same way as in C for moving inside the file, all methods listed below will, like in C, move the cursor,<br>
    so for example the *read(10)* method will move the cursor 10 bytes forward (see examples below).<br><br>
    There's also a method to move the cursor that's called *seek(5)*, where *5* will be the amount of bytes you will move the cursor inside the file.<br>
    You can also put negative numbers to move the cursor backward.
        
* Reading a file :

    The *dc.read()* function allows you to read a file. If you don't specify any parameters, it will return the entire file as a string.
    You can put one parameter inside it which will be the amount of characters read inside the file.
    * Example : dc.read(5) will read 5 characters from the cursor in the file with the dc descriptor.
        
    <br>Alternatively, you can use the *dc.readline()* function which will return you an entire line (in function of the cursor's position).
        
* Writing in a file :

    The *dc.write()* function allows you to write into a file.
    * Example : dc.write("Hello world") will write into the file with the dc descriptor the string "Hello world", starting from the cursor's position

*author: DE ALMEIDA Jonathan G2S4*


## Generating random number
In order to generate random number, you will need to import the `random` library:
```python
import random
```

And then use the library this way:
```python
# Get a random number between 1 and 12
random.randint(1, 12)

# Get a float between 0 and 1
random.random()
```

Authors: Mathieu Maisonnette, Pierre Hubert

## Running a Python file through a Shell script

*  You may need to write in Bash for some of your programs or functions, but there's a way to avoid it if you're not comfortable with this language and if you prefer Python.
*  You can run .py files using the following function in Bash :
        
    `python3 path/to/the/python_script.py`

*author: DE ALMEIDA Jonathan G2S4*