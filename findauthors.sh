#!/bin/bash

getauthors() {
IFS=$'\n'
printf '# Authors\n'
printf '| page | authors |\n'
printf '|:---- |:------- |\n'
for f in `find docs -name '*.md' -print | cut -d/ -f2- | egrep -v '^Authors.md$' | sort`; do
	printf '| `%s` |' "$f"
	FIRST=1
	for a in `git blame -pw "docs/$f" | egrep '^author ' | cut -d' ' -f2- | sed 's/p[0-9][0-9]*//' | sort | uniq`; do
		for r in `cat authorexceptions`; do
			p=`echo $r | cut -d: -f1`
			[ "$a" = "$p" ] && a=`echo $r | cut -d: -f2-`
		done
		[ '1' = $FIRST ] || printf ','
		printf ' %s' "$a"
		FIRST=0
	done
	printf ' |\n'
done
}

getauthors > docs/Authors.md
