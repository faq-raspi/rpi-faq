# Rpi FAQ


## Contributing
Choose the categorie you want to work on.

Share your tips, advice & screenshots.
Evertything will eventually be shared on a website hosted on the University servers.

The Markdown syntax is used to write the documentation, which is then built using `mkdocs`, an open-source documentation tool.


To submit your modifications, ask your English teacher for access (you'll need to have the `Developer` role to update the project).

Keep in mind your work will be useful to future S3 students. 

The website is live at http://faq-raspi.pages.univ-lyon1.fr/rpi-faq/


**ALL THE DOCUMENTATION MUST BE INCLUDED IN THE `docs` DIRECTORY IN ORDER TO BE SERVED!!!** 


## Upload images
* First, upload the image inside the same directory of the Markdown (md) file you target
* In the markdown file, add the image using this syntax: `![](path_to_image.png)`. Do not forget the extension! The image should be automatically included in the website.
